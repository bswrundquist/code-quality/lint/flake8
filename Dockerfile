FROM python:3.8-alpine

RUN pip install flake8

ENV PYTHONUNBUFFERED True

WORKDIR /opt
COPY . .

ENTRYPOINT [ "" ]