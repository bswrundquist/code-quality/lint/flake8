FQN := bswrundquist-code-quality-lint-flake8

build:
	docker build -t ${FQN} .

smoke-test: build
	docker run -it --rm ${FQN} flake8 test.py

help: build
	docker run -it --rm ${FQN} --help

refresh:
	docker stop ${FQN} || true
	docker rm ${FQN} || true
	docker rmi ${FQN} || true

clean:
	rm -rf .coverage
	rm -rf .idea
	rm -rf  __pycache__